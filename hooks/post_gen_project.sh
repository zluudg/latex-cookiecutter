#!/bin/bash

git init -b main

git config --local user.name {{cookiecutter.commit_name}}
git config --local user.email {{cookiecutter.commit_email}}

curl https://raw.githubusercontent.com/github/gitignore/master/TeX.gitignore > .gitignore
echo "" >> .gitignore
echo "# Ignore PDF files by default" >> .gitignore
echo "*.pdf" >> .gitignore
echo "# Ignore latest pointer in logbook dir" >> .gitignore
echo "logbook/latest" >> .gitignore

if [ ! -z "$GITLAB_API_PRIVATE_TOKEN" ]
then
    curl -H "Content-Type:application/json" https://$GITLAB_URL/api/v4/projects\?private_token\=$GITLAB_API_PRIVATE_TOKEN -d "{\"name\": \"{{cookiecutter.doc_title}}\", \"path\": \"{{cookiecutter.project_slug}}\", \"visibility\": \"{{cookiecutter.project_visibility}}\"" | jq
    git remote add origin git@$GITLAB_URL:{{cookiecutter.gitlab_name}}/{{cookiecutter.project_slug}}.git
fi

git add .
git commit -m "Initial commit"

REFS_URL={{cookiecutter.references_url}}
if [ -z "$REFS_URL"]
then
    mkdir bibtex-references
    touch bibtex-references/main.bib
else
    git submodule add "$REFS_URL"
    git commit -am "Added submodule $REFS_URL"
fi

if [ ! -z "$GITLAB_API_PRIVATE_TOKEN" ]
then
    git push -u origin main
fi
